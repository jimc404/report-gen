## Pseudocode for the report PDF generation

#### Basic idea:
```
Data(fetched from DB)
        |
        |
        | supplied to renderer (EJS)
        |
        V
  ------------------------------------------
 | Rendered HTML report                    |
 |   steps followed:                       |
 |   1. fill customer data                 |
 |   2. Generate chart                     |
 |   3. Generate blood pressure log table  |
 -------------------------------------------
        |
        |
        | converting HTML to PDF (each page is rendered separately and stitched together in the end)
        |
        V
    Required PDF report
```

#### Pseudocode:
```
// Driver function
generateReport(customerData: JSON):
    // first generating chart
    chart = generateChart(customerData) // returns the path to chart image
    
    patientDetails = extractDetails(customerData) // gets details like name/age/email in one object

    bpLog = extractBPLog(customerData)
    // Returns an array of arrays that contain BP stats in groups of 20 or less
    // [[...], [...], [...] ... ] each inner array has 20 elements as each page can have at most 20 rows

    report = generatePDF(chart,bpLog,patientDetails)
    // report is the name/path to the generated file

    deleteFile(image) // as report PDF is generated the image can be removed

    return report

```
#### Notes
- PDF generation can be done with `pdf-puppeteer` module, it's a wrapper around puppeteer. It benefits by generating PDFs exactly as they appear in HTML (unlike wkhtml2pdf or similar libraries)
- Using puppeteer can help as the chart can be made with over the shelf libraries like chartJS or d3 JS and there won't be a need to render png chart and embed them